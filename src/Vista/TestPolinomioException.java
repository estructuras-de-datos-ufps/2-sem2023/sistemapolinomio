/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.SistemaPolinomio;

/**
 *
 * @author madarme
 */
public class TestPolinomioException {

    public static void main(String[] args) {
        String c1 = "3&;3&x&6";
        String c2 = "3&x&2;12&z&3";
        try {
            SistemaPolinomio myPolinomios = new SistemaPolinomio(c1, c2);
            System.out.println(myPolinomios.toString());
        } catch (Exception error) {
            System.err.println(error.getMessage());
        }
    }
}
