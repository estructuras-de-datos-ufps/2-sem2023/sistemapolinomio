/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.SistemaPolinomio;

/**
 *
 * @author docente
 */
public class TestPolinomio {
    public static void main(String[] args) {
        String c1="3&;-2&y&3";
        String c2="3&x&2;12&z&3";
       
        SistemaPolinomio myPolinomios=new SistemaPolinomio(c1, c2);
        System.out.println(myPolinomios.toString());
    }
}
