/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author docente
 */
public class Polinomio {

    private Termino terminos[];

    public Polinomio() {
    }

    /**
     * Construye un polinomio a través de una cadena del tipo
     * coeficiente&literal&grado; ... Ejemplo: 3&x&2;4&y&1 --> 3x2+4y1
     *
     * @param datos una cadena formateada con los datos del polinomio
     */
    public Polinomio(String datos) {

        if (datos.isEmpty() || datos == null) {
            throw new RuntimeException("Cadena vacía");
        }

        // datos=3&x&2;4&y&1 --> 3x2+4y1
        String ter[] = datos.split(";");
        //terminos={(3&x&2); (4&y&1)}
        //Crear el espacio para los términos:
        this.terminos = new Termino[ter.length];
        for (int i = 0; i < this.terminos.length; i++) {

            String dato = ter[i]; //3&x&2

            //Partirlo
            String dato2[] = dato.split("&");
//            if (dato2.length == 1) {
//                throw new RuntimeException("No es un token válido");
//            }

            //dato2={3,x,2}
            try {
                float coef = Float.parseFloat(dato2[0]);
                char literal = dato2[1].charAt(0);
                float grado = Float.parseFloat(dato2[2]);
                Termino nuevo = new Termino(coef, literal, grado);
                this.terminos[i] = nuevo;
            } catch (Exception error) {
                throw new RuntimeException("Error número");

            }
        }
    }

    public Termino[] getTerminos() {
        return terminos;
    }

    public void setTerminos(Termino[] terminos) {
        this.terminos = terminos;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Termino dato : this.terminos) {
            msg += dato.toString() + "\t";
        }
        return msg;
    }

    public Polinomio getSumar(Polinomio p2) {
        return null;
    }

}
