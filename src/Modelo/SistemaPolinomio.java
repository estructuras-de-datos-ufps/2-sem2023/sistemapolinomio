/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author docente
 */
public class SistemaPolinomio {

    Polinomio p1, p2;

    public SistemaPolinomio() {
    }

    public SistemaPolinomio(String cadena1, String cadena2) {
        this.p1 = new Polinomio(cadena1);
        this.p2 = new Polinomio(cadena2);

    }

    public Polinomio getP1() {
        return p1;
    }

    public void setP1(Polinomio p1) {
        this.p1 = p1;
    }

    public Polinomio getP2() {
        return p2;
    }

    public void setP2(Polinomio p2) {
        this.p2 = p2;
    }

    @Override
    public String toString() {
        return this.p1.toString() + "\n" + this.p2.toString();
    }

    public Polinomio getSumar()
    {
        return null;
    }
    
    public Polinomio getRestar()
    {
        return null;
    }
    
    public void derivar1()
    {
    
    }
    
    public void integrar1()
    {
    
    }
    
     public void derivar2()
    {
    
    }
    
    public void integrar2()
    {
    
    }
    
    
}
