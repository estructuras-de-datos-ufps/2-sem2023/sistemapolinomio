/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author docente
 */
public class Termino {

    private float coeficiente;
    private char literal;
    private float grado;

    public Termino() {
    }

    public Termino(float coeficiente, char literal, float grado) {
        this.coeficiente = coeficiente;
        this.literal = literal;
        this.grado = grado;
    }

    public float getCoeficiente() {
        return coeficiente;
    }

    public void setCoeficiente(float coeficiente) {
        this.coeficiente = coeficiente;
    }

    public char getLiteral() {
        return literal;
    }

    public void setLiteral(char literal) {
        this.literal = literal;
    }

    public float getGrado() {
        return grado;
    }

    public void setGrado(float grado) {
        this.grado = grado;
    }

    @Override
    public String toString() {
        String signo=this.coeficiente>=0?"+":"";
        return coeficiente+""+literal +"^"+grado+signo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Float.floatToIntBits(this.coeficiente);
        hash = 37 * hash + this.literal;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        //MiddlerCasting:
        final Termino other = (Termino) obj;
        return (this.literal == other.literal) && other.grado == this.grado;
    }

    public Termino getSumar(Termino dos)
    {
        return null;
    }
    
    public Termino getRestar(Termino dos)
    {
        return null;
    }
    
    public void derivar()
    {
        
    }
    
    public void integrar()
    {
        
    }

}
